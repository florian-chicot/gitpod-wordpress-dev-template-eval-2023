<?php
function iut_wp_enqueue_scripts() {
  $parenthandle = 'twentynineteen-style';
  $theme        = wp_get_theme();

  // Load parent CSS
  wp_enqueue_style( 
      $parenthandle,
    get_template_directory_uri() . '/style.css', // url du fichier css du thème parent
    array(),
    $theme->parent()->get( 'Version' )
  );


  // Load CSS child
  wp_enqueue_style( 
      'iut-style',
    get_stylesheet_uri(), // url du fichier css du thème enfant
    array( $parenthandle ),
    $theme->get( 'Version' )
  );
}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );

// Type de contenu Recette
function iut_register_post_type_project()
{
  register_post_type(
    'recipe',
    array(
      'labels' => array(
        'name' => __('Recettes'),
        'singular_name' => __('Recette'),
      ),
      'has_archive' => 'recettes',
      'herarchical' => false,
      'menu_icon' => 'dashicons-food',
      'public' => true,
      'publicly_queryable' => true,
      'rewrite' => array('slug' => 'recipe'),
      'show_in_rest' => true, // permet au type de contenu d'utiliser l'editeur Gutenberg
      'supports' => array('title', 'editor'),
    )
  );
}

add_action('init', 'iut_register_post_type_project', 10);

// Metabox au type de contenu Recette
function iut_add_meta_boxes_recipe($post) {
  add_meta_box(
    'iut_mbox_recipe',          // Unique ID
    'Infos complémentaires',    // Box title
    'iut_mbox_recipe_content',  // Content callback, must be of type callable
    'recipe'                    // Post type
  );
}

add_action('add_meta_boxes', 'iut_add_meta_boxes_recipe');

function iut_mbox_recipe_content($post) {
  // Get meta value
  $iut_ingredients = get_post_meta(
    $post->ID,
    'iut-ingredients',
    true
  );

  echo '
  <p>
    <label for="iut-ingredients">Ingrédients : </label>
    <textarea name="iut-ingredients" id="iut-ingredients" cols="64" rows="4">'. $iut_ingredients .'</textarea>
  </p>
  ';
}

// Save post meta
function iut_save_post($post_id) {
  if (isset($_POST['iut-ingredients']) && !empty($_POST['iut-ingredients'])) {
    update_post_meta(
      $post_id,
      'iut-ingredients',
      sanitize_textarea_field($_POST['iut-ingredients'])
    );
  }
}

add_action('save_post', 'iut_save_post');